THIS_FILE := $(lastword $(MAKEFILE_LIST))
AWSPROFILE=reppard
WEBSRCDIR=web
TFDIR=tf
SHELL := /bin/bash
#env?=dev

#export ENV=$(env)
###############################################################################
# Terraform targets
###############################################################################

tf-init:
	cd $(TFDIR) \
		&& AWS_PROFILE=$(AWSPROFILE) terraform init -input=true # \
		# && AWS_PROFILE=$(AWSPROFILE) terraform workspace select $(env)

tf-output: tf-init
	cd $(TFDIR) \
		&& AWS_PROFILE=$(AWSPROFILE) terraform output

tf-plan: tf-init
	cd $(TFDIR) \
		&& AWS_PROFILE=$(AWSPROFILE) terraform plan

tf-apply: tf-init
	cd $(TFDIR) \
		&& AWS_PROFILE=$(AWSPROFILE) terraform apply
	$(MAKE) -f $(THIS_FILE) invalidate-distro
	#$(MAKE) -f $(THIS_FILE) env=$(env) invalidate-distro

invalidate-distro: tf-init
	$(eval DIST_ID := $(shell cd $(TFDIR) && AWS_PROFILE=$(AWSPROFILE) terraform output distribution_id))
	aws --profile=$(AWSPROFILE) cloudfront create-invalidation \
		--distribution-id $(DIST_ID) --paths '/*'
