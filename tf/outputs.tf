output "chickenshit_website_domain" {
	value = "${aws_s3_bucket.chickenshit.website_domain}"
}

output "chickenshit_bucket_domain_name" {
	value = "${aws_s3_bucket.chickenshit.bucket_domain_name}"
}

output "distribution_id" {
  value = aws_cloudfront_distribution.chickenshit.id
}
