resource "aws_s3_bucket" "chickenshit" {
  bucket = var.root_domain
  acl    = "public-read"

  website {
    index_document = "index.html"
    error_document = "index.html"
  }
}

resource "aws_s3_bucket_policy" "chickenshit" {
  bucket = aws_s3_bucket.chickenshit.id
  policy = data.aws_iam_policy_document.chickenshit.json
}

data "aws_iam_policy_document" "chickenshit" {
  statement {
    actions = ["s3:GetObject"]

    resources = ["${aws_s3_bucket.chickenshit.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
  }
}

resource "aws_s3_bucket_object" "chickenshit_html" {
  for_each = fileset(path.module, "../web/*.html" )
  content_type = "text/html"

  bucket   = aws_s3_bucket.chickenshit.id
  key      = replace(each.value, "../web/", "")
  etag     = base64sha256(file(each.value))
  source   = each.value
  acl      = "public-read"
}

resource "aws_s3_bucket_object" "chickenshit_js" {
  for_each = fileset(path.module, "../web/**/*.js" )
  content_type = "application/js"

  bucket   = aws_s3_bucket.chickenshit.id
  key      = replace(each.value, "../web/", "")
  etag     = base64sha256(file(each.value))
  source   = each.value
  acl      = "public-read"
}


resource "aws_s3_bucket_object" "chickenshit_css" {
  for_each = fileset(path.module, "../web/**/*.css")
  content_type = "text/css"

  bucket   = aws_s3_bucket.chickenshit.id
  key      = replace(each.value, "../web/", "")
  etag     = base64sha256(file(each.value))
  source   = each.value
  acl      = "public-read"
}

resource "aws_s3_bucket_object" "chickenshit_img" {
  for_each = fileset(path.module, "../web/img/*.svg")
  content_type = "image/svg+xml"

  bucket   = aws_s3_bucket.chickenshit.id
  key      = replace(each.value, "../web/", "")
  etag     = base64sha256(file(each.value))
  source   = each.value
  acl      = "public-read"
}

resource "aws_s3_bucket_object" "chickenshit_png" {
  for_each = fileset(path.module, "../web/img/*.png")
  content_type = "image/png"

  bucket   = aws_s3_bucket.chickenshit.id
  key      = replace(each.value, "../web/", "")
  etag     = base64sha256(filebase64(each.value))
  source   = each.value
  acl      = "public-read"
}

resource "aws_s3_bucket_object" "chickenshit_fonts" {
  for_each = fileset(path.module, "../web/fonts/*")
  content_type = "font"

  bucket   = aws_s3_bucket.chickenshit.id
  key      = replace(each.value, "../web/", "")
  source   = each.value
  acl      = "public-read"
}

resource "aws_s3_bucket_object" "chickenshit_favicon" {
  content_type = "image/vnd.microsoft.icon"

  bucket   = aws_s3_bucket.chickenshit.id
  key      = "favicon.ico"
  source   = "${path.module}/../web/favicon.ico"
  acl      = "public-read"
}
