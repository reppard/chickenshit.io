locals {
    s3_origin_id = "${var.root_domain} Origin"
}

resource "aws_cloudfront_distribution" "chickenshit" {
    origin {
        domain_name = aws_s3_bucket.chickenshit.bucket_regional_domain_name
        origin_id   = local.s3_origin_id
    }

    price_class = "PriceClass_100"

    default_cache_behavior {
        allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
        cached_methods   = ["GET", "HEAD"]
        target_origin_id = local.s3_origin_id

        forwarded_values {
            query_string = false
            headers = ["Authorization","Accept","Origin"]

            cookies {
                forward = "none"
            }
        }

        viewer_protocol_policy = "redirect-to-https"
        min_ttl                = 0
        default_ttl            = 3600
        max_ttl                = 86400
    }

    enabled             = true
    default_root_object = "index.html"

    custom_error_response {
        error_code = 404
        response_code = 200
        response_page_path = "/index.html"
    }

    custom_error_response {
        error_code = 403
        response_code = 200
        response_page_path = "/index.html"
    }

    aliases             = [var.root_domain]
    tags                = {
        Environment = "prod"
    }

    restrictions {
        geo_restriction {
            restriction_type = "whitelist"
            locations        = ["US"]
        }
    }

    viewer_certificate {
        acm_certificate_arn = aws_acm_certificate.chickenshit.arn
        ssl_support_method = "sni-only"
    }
}
