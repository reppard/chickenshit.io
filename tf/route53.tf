resource "aws_route53_zone" "chickenshit" {
  name         = var.root_domain
}

resource "aws_route53_record" "chickenshit" {
  zone_id = aws_route53_zone.chickenshit.zone_id
  name    = var.root_domain
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.chickenshit.domain_name
    zone_id                = aws_cloudfront_distribution.chickenshit.hosted_zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "chickenshit_validation" {
	name    = aws_acm_certificate.chickenshit.domain_validation_options.0.resource_record_name
	type    = aws_acm_certificate.chickenshit.domain_validation_options.0.resource_record_type
	zone_id = aws_route53_zone.chickenshit.id
	records = [aws_acm_certificate.chickenshit.domain_validation_options.0.resource_record_value]
	ttl     = 60
}

resource "aws_acm_certificate" "chickenshit" {
    domain_name       = var.root_domain
    validation_method = "DNS"
}

resource "aws_acm_certificate_validation" "chickenshit" {
	certificate_arn = aws_acm_certificate.chickenshit.arn
	validation_record_fqdns = [aws_route53_record.chickenshit_validation.fqdn]
}
