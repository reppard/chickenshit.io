variable "region" {
  default = "us-east-1"
}

variable "root_domain" {
  default = "chickenshit.io"
}

variable "app" {
  default = "chickenshit"
}

output "name_servers" {
    value = aws_route53_zone.chickenshit.name_servers
}
