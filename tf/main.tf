terraform {
  required_version = ">= 0.12.13"
  backend "s3" {
    bucket = "chickenshit-terraform-state"
    key    = "link-aggregator/terraform.tfstate"
    region = "us-east-1"
  }
}

provider "aws" {
    version = "2.68"
    region = "us-east-1"
}
