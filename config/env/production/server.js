module.exports = ({ env }) => ({
  cron: {
    enabled: true
  },
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '64699b2cbaa021bd5fcb32d6d4db47e0'),
    },
  },
});
