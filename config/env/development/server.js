module.exports = ({ env }) => ({
  cron: {
    enabled: true
  },
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1338),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '9339862447679468d0c89bdd19a64942'),
    },
  },
});
