'use strict';
const axios = require('axios');

/**
 * Cron config that gives you an opportunity
 * to run scheduled jobs.
 *
 * The cron format consists of:
 * [SECOND (optional)] [MINUTE] [HOUR] [DAY OF MONTH] [MONTH OF YEAR] [DAY OF WEEK]
 *
 * See more details here: https://strapi.io/documentation/v3.x/concepts/configurations.html#cron-tasks
 */
module.exports = {
    '*/5 * * * *': async () => {
        console.log("running cron")
        axios.get("https://chickenshit.io").then(resp => console.log(resp))
    }
}
