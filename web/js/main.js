var config = {
    //apiEndpoint: "http://localhost:1337/"
    apiEndpoint: "https://link-aggregator-backend.herokuapp.com/"
}

const setBase = () => {
    let base = document.getElementById("base")

    if(document.location.href.match("chickenshit.io")) {
        base.href = "https://chickenshit.io/"
    } else {
        let root = document.location.pathname.split("/").filter(x=> !x.match("html")).join("/")
        base.href = document.location.origin + root + "/"
    }

}

const sessionData = {
    username: window.sessionStorage.getItem("aggusername"),
    userid: window.sessionStorage.getItem("agguserid"),
    jwt: window.sessionStorage.getItem("aggjwt"),
}

const urlDomain = (url) => {
    var a = document.createElement('a')
    a.href = url
    return a.hostname
}

const userOwned = (story) => {
    return story.user.id == sessionData.userid ? 'inline' : 'none'
}

const handleDelete = async (id) => {
    let response = await fetch(config.apiEndpoint + "stories/" + id, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': `Bearer ${sessionData.jwt}`
        },
    });

    if (response.ok){
        let json = await response.json();
        data = json
        window.location.reload()
    } else {
        console.log(response)
    }
}

const getDataListItems = (data) => {
    let listItems = ""
    let sorted = data.sort((x,y) => {return y.votes.length - x.votes.length})

    if(sorted.length > 0){
        for(let i in sorted) {
            let story = sorted[i]
            let domain = urlDomain(story.url)
            let points = story.votes.length
            let created_at = new Date(story.created_at)
            let date = created_at.toLocaleDateString()
            let time = created_at.toLocaleTimeString()

            listItems +=
            `<li class="story" id="${story.id}">

                <div class="votes">
                    <span
                        class="fa fa-angle-double-up"
                        onclick="castVote(${story.id}, 1)"></span>
                    <div class="points">
                        <span class="points">${points}</span>
                    </div>
                </div>

                <div>
                    <div class="details-top">
                        <div style="display: ${userOwned(story)};" class="delete">
                            <div
                                class="fa fa-trash-o clickable"
                                onclick="handleDelete(${story.id})"></div>
                        </div>
                        <span>
                            <a href="${story.url}">${story.title}</a>
                        </span>
                        <span>
                            <a class="story-domain" href="http://${domain}">
                                (${domain})
                            </a>
                        </span>
                    </div>
                    <div class="details-bottom">
                        <span>
                            ${date} - ${time}
                        </span>
                        <span>
                            by ${story.user.username}
                        </span>
                    </div>
                </div>
            </li>
            `
        }

        return listItems
    } else {
        return "<h2>No data found.</h2>"
    }
}

const castVote = async (story_id, value) => {
    let response = await fetch(config.apiEndpoint + "votes", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': `Bearer ${sessionData.jwt}`
        },
        body: JSON.stringify(
            { "story": { "id": story_id } }
        ),
    })

    if (response.ok){
        let json = await response.json();
        data = json
        window.location.reload()
    } else {
        let json = await response.json()
        data = json
        msg = data.message
        let alertbox = document.getElementById("alertbox")
        alertbox.innerHTML = `<h3>${data.message}</h3>`
        setTimeout(() => {alertbox.innerHTML = ""}, 3000)
    }
}

const submitData = async (data) => {
    let response = await fetch(config.apiEndpoint + "stories", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': `Bearer ${sessionData.jwt}`
        },
        body: JSON.stringify(data),
    });

    if (response.ok){
        let json = await response.json();
        data = json
        window.location.reload()
    } else {
        console.log(response)
    }

}

const loadStories = async () => {
    let storyContainer = document.getElementById("story-list")

    if(storyContainer){
        storyContainer.innerHTML = `
            <div class="box">
                <h3>Loading...</h3>
                <!--<p>
                    Please wait. This site is currently under development and is
                    being hosted on free tier heroku infrastructure for the time
                    being. As such it might be that the servers have been put into
                    an inactive state and are currently spinning back up. Feel free
                    to register an account and explore but please not that there is
                    no guarantee that accounts and data will persist once alpha
                    development is completed. Thanks.
                </p>-->
            </div>
        `

        let response = await fetch(config.apiEndpoint + "stories")
        let data = []

        if (response.ok) {
            let json = await response.json();
            data = json

            storyContainer.innerHTML = getDataListItems(data)
        } else {
            console.log(response)
            storyContainer.innerHTML = `<h2>HTTP-Error: ${response.status}</h2>`
        }
    }
}

const loadSubmitListener = async () => {
    let submitForm = document.querySelector('form')

    if(submitForm) {
        submitForm.addEventListener('submit', (e) => {
            e.preventDefault()
            e.stopPropagation()
            let formData = Object.fromEntries(new FormData(e.target).entries())
            submitData(formData)
        })
    }
}

const handleLogin = async (data) => {
    let response = await fetch(config.apiEndpoint + "auth/local", {
          method: 'POST',
          headers: {
                  'Content-Type': 'application/json;charset=utf-8'
                },
          body: JSON.stringify(data)
    });

    if (response.ok){
        let json = await response.json()
        data = json
        window.sessionStorage.setItem("aggjwt", data.jwt)
        window.sessionStorage.setItem("agguserid", data.user.id)
        window.sessionStorage.setItem("aggusername", data.user.username)
        window.location = window.location.href.split("login.html")[0] + "index.html"
    } else {
        let json = await response.json()
        data = json
        msg = data.message[0].messages[0].message
        let alertbox = document.getElementById("alertbox")
        alertbox.innerHTML = `<h3>${msg}</h3>`
        setTimeout(() => {alertbox.innerHTML = ""}, 3000)
    }

}

const handleRegister = async (data) => {
    console.log(data)
    let alertbox = document.getElementById("alertbox")
    alertbox.innerHTML = "<h3>Loading...</h3>"
    let response = await fetch(config.apiEndpoint + "auth/local/register", {
          method: 'POST',
          headers: {
                  'Content-Type': 'application/json;charset=utf-8'
                },
          body: JSON.stringify(data)
    });

    if (response.ok){
        let json = await response.json()
        console.log(JSON.stringify(json))
        data = json
        window.sessionStorage.setItem("aggjwt", data.jwt)
        window.sessionStorage.setItem("agguserid", data.user.id)
        window.sessionStorage.setItem("aggusername", data.user.username)
        window.location = window.location.href.split("login.html")[0] + "index.html"
    } else {
        let json = await response.json()
        data = json
        msg = data.message[0].messages[0].message
        alertbox.innerHTML = `<h3>${msg}</h3>`
        setTimeout(() => {alertbox.innerHTML = ""}, 3000)
    }
}

const handleLogout = () => {
    window.sessionStorage.clear()
    window.location.reload()
}

const loadLoginListener = () => {
    let loginForm = document.getElementById('login-form')
    let registerForm = document.getElementById('register-form')

    if(loginForm) {
        loginForm.addEventListener('submit', (e) => {
            e.preventDefault()
            let formData = Object.fromEntries(new FormData(e.target).entries())
            handleLogin(formData)
        })
    }

    if(registerForm) {
        renderQuiz()
        let answer = document.getElementById('answer')

        answer.addEventListener('change', (e) => {
            let answer = getQuizAnswer()
            let value = e.currentTarget.value

            if(value == answer) {
                let button = document.getElementById("register")
                button.disabled = false
            } else {
                button.disabled = true
            }
        })

        registerForm.addEventListener('submit', (e) => {
            e.preventDefault()
            e.stopPropagation()
            let formData = Object.fromEntries(new FormData(e.target).entries())
            handleRegister(formData)
        })
    }
}

const renderQuiz = () => {
    let quizNums = document.getElementsByClassName('quiz_numbers')

    for(num in quizNums) {
        quizNums[num].textContent = Math.floor(Math.random() * 20) + 1
    }
}

const getQuizAnswer = () => {
    let vals = []
    let quiz = document.getElementsByClassName('quiz_numbers')

    for(num in quiz) {
        let number = parseInt(quiz[num].innerText)
        if(number){
            vals.push(number)
        }
    }

    return vals.reduce((a,b) => a+b)
}

const getPage = () => {
    let locationArr = window.location.href.split("/")
    return locationArr[locationArr.length - 1]
}

const toggleSubmitForm = () => {
    let sub = document.getElementById("submission-area")

    if (sub.style.display === "none") {
        sub.style.display = "block"
    } else {
        sub.style.display = "none"
    }

}

const handleSession = () => {
    if(sessionData.jwt != null){
        let loginDiv = document.getElementById("login")

        loginDiv.innerHTML = `
           ${sessionData.username}
           <div class="fa fa-sign-out clickable" onclick="handleLogout()"></div>
        `
    } else {
        let sub = document.getElementById("submission-area")
        if(sub){
            sub.innerHTML = `<h3>
                Please <a style="color: red;" href="login.html">login</a>...
            </h3>`
        }
    }
}

const init = () => {
    setBase()
    handleSession()
    loadStories()
    loadSubmitListener()
    loadLoginListener()
}

window.onload = init()
