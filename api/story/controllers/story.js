'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

const { sanitizeEntity } = require('strapi-utils');

module.exports = {
    /**
     *    * Create a record.
     *       *
     *          * @return {Object}
     *             */

    async create(ctx) {
        let entity;
        ctx.request.body.user = ctx.state.user.id;
        entity = await strapi.services.story.create(ctx.request.body);
        return sanitizeEntity(entity, { model: strapi.models.story });
    },
    async update(ctx) {
        const { id } = ctx.params;

        let entity;

        const [story] = await strapi.services.story.find({
            id: ctx.params.id,
            'user.id': ctx.state.user.id,
        });

        if (!story) {
            return ctx.unauthorized(`You can't update this entry`);
        }

        entity = await strapi.services.story.update({ id }, ctx.request.body);

        return sanitizeEntity(entity, { model: strapi.models.story });
    },
    async delete(ctx) {
        const { id } = ctx.params;

        let entity;

        const [story] = await strapi.services.story.find({
            id: ctx.params.id,
            'user.id': ctx.state.user.id,
        });

        if (!story) {
            return ctx.unauthorized(`You can't delete this entry`);
        }

        entity = await strapi.services.story.delete({ id });

        return sanitizeEntity(entity, { model: strapi.models.story });
    },
};
