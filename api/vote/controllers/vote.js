'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

const { sanitizeEntity } = require('strapi-utils');

module.exports = {
    /**
     *    * Create a record.
     *       *
     *          * @return {Object}
     *             */

    async create(ctx) {
        let entity;

        ctx.request.body.user = ctx.state.user.id;

        const [vote] = await strapi.query('vote').find({
            'story.id': ctx.request.body.story.id,
            'user.id': ctx.state.user.id,
        });

        if (vote) {
            console.log(vote)
            return ctx.unauthorized(`Already voted!`);
        }

        entity = await strapi.services.vote.create(ctx.request.body);

        return sanitizeEntity(entity, { model: strapi.models.vote });
    }
};
